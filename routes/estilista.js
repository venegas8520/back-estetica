//Ruta para estlista
const express = require('express');
const router = express.Router();
const estilistaController = require('../controllers/estilistaController')

//api/Estilistas
router.post('/', estilistaController.crearEstilista);
router.get('/', estilistaController.obtenerEstilistas);
router.put('/:id', estilistaController.actualizarEstilista);
router.get('/:id', estilistaController.obtenerEstilista);
router.delete('/:id', estilistaController.eliminarEstilista); 

module.exports = router;