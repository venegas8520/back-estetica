//Ruta para servicio
const express = require('express');
const router = express.Router();
const servicioController = require('../controllers/servicioController')

//api/servicios
router.post('/', servicioController.crearServicio);
router.get('/', servicioController.obtenerServicios);
router.put('/:id', servicioController.actulizarServicio);
router.get('/:id', servicioController.obtenerServicio);
router.delete('/:id', servicioController.eliminarServicio);  

module.exports = router;