//Ruta para citas
const express = require('express');
const router = express.Router();
const citasController = require('../controllers/citaController')

//api/citas
router.post('/', citasController.crearCita);
router.get('/', citasController.obtenerCitas);
router.put('/:id', citasController.actulizarCita);
router.get('/:id', citasController.obtenerCita);
router.delete('/:id', citasController.eliminarCita);

module.exports = router;