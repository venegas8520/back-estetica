//Ruta para citas
const express = require('express');
const router = express.Router();
const usuarioController = require('../controllers/usuarioController')

//api/usuarios
router.post('/',usuarioController.crearUser );
router.get('/', usuarioController.obtenerUsers);
router.put('/:id', usuarioController.actualizarUser);
router.get('/:id', usuarioController.obtenerUser);
router.delete('/:id', usuarioController.eliminarUser); 

module.exports = router;