const mongoose = require('mongoose');

const citaSchema = mongoose.Schema({
    fecha: {
        type: String,
        required: true
    },
    hora: {
        type: String,
        required: true
    },
    ubicacion: {
        type: String
    },
    servicio: {
        type: String,
        required: true
    },
    nombre: {
        type: String
    },
    apellidos: {
        type: String
    },
    telefono: {
        type: Number
    },
    estado: {
        type: Boolean
    }
    
});

module.exports = mongoose.model('cita', citaSchema);