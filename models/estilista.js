const mongoose = require('mongoose');

const estlistaSchema = mongoose.Schema({
    nombre: {
        type: String,
        required: true
    },
    apellido: {
        type: String,
        required: true
    },
    edad: {
        type: Number,
        required: true
    },
    correo: {
        type: String,
        required: true
    },
    telefono: {
        type: Number,
        required: true
    },
    especialidad: {
        type: String,
        require: true
    },
    direccion: {
        type: String,
        required: true
    },
    foto: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('estilista', estlistaSchema);