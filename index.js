const express = require('express');
const conectarDB = require('./config/database')
const cors = require("cors");

//Creamos el servidor
const app = express();

//Conectamos a la base de datos
conectarDB();

//agregamos cors
app.use(cors())

//Habilitamos que se pueda recibir json
app.use(express.json());

//Definimos la ruta para las citas
app.use('/api/citas', require('./routes/citas'));
//Definimos la ruta para los usuarios
app.use('/api/usuarios', require('./routes/usuarios'));
//definimos ruta para estilistas
app.use('/api/estilistas', require('./routes/estilista'));
//definimos la ruta para servicios
app.use('/api/servicios', require('./routes/servicio'));
//definimos la ruta para productos
app.use('/api/productos', require('./routes/productos'));

//Definimos ruta principal
app.get('/', (req, res) => {
    res.send("Proyecto estetica");
})

app.listen(4000, () => {
    console.log("El servidor esta corriendo en el puerto 4000");
})