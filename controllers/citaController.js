const cita = require("../models/cita");

//Creamos cita
exports.crearCita = async(req, res) => {

    try {
        let Cita;
        //creamos la cita
        Cita = new cita(req.body);

        await Cita.save();
        res.send(Cita);

    } catch (error) {

        console.log(error);
        res.status(500).send('hubo un error');
    }
}

//Obtenemos citas
exports.obtenerCitas = async(req, res) => {

    try {
        const citas = await cita.find();
        res.json(citas)
    } catch (error) {
        console.log(error);
        res.status(500).send('hubo un error');
    }
}

//Actualizamos cita
exports.actulizarCita = async(req, res) => {

    try {
        const { fecha, hora, ubicacion, servicio } = req.body;
        let Cita = await cita.findById(req.params.id);
        if (!Cita) {
            res.status(404).json({ msg: "no existe la cita" })
        }

        Cita.fecha = fecha;
        Cita.hora = hora;
        Cita.ubicacion = ubicacion;
        Cita.servicio = servicio;

        Cita = await cita.findOneAndUpdate({ _id: req.params.id }, Cita, { new: true })
        res.json(Cita);
    } catch (error) {
        console.log(error);
        res.status(500).send('hubo un error');

    }
}

// obtenemos una cita
exports.obtenerCita = async(req, res) => {

    try {
        let Cita = await cita.findById(req.params.id);
        if (!Cita) {
            res.status(404).json({ msg: "no existe la cita" })
        }

        res.json(Cita);

    } catch (error) {
        console.log(error);
        res.status(500).send('hubo un error');

    }
}

//Eliminamos cita
exports.eliminarCita = async(req, res) => {

    try {
        let Cita = await cita.findById(req.params.id);

        if (!Cita) {
            res.status(404).json({ msg: "no existe la cita" })
        }

        await cita.findOneAndRemove({ _id: req.params.id })
        res.json({ msg: "Cita eliminada con exito" });

    } catch (error) {
        console.log(error);
        res.status(500).send('hubo un error ');

    }
}