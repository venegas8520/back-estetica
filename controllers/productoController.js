const product = require("../models/producto");

//Creamos producto
exports.crearProducto = async(req, res) => {

    try {
        let producto;
        //creamos el producto
        producto = new product(req.body);

        await producto.save();
        res.send(producto);

    } catch (error) {

        console.log(error);
        res.status(500).send('hubo un error');
    }
}

//Obtenemos productos
exports.obtenerProductos = async(req, res) => {

    try {
        const producto = await product.find();
        res.json(producto)
    } catch (error) {
        console.log(error);
        res.status(500).send('hubo un error');
    }
}

//Actualizamos producto
exports.actulizarProducto = async(req, res) => {

    try {
        const { nombre, descripcion, precio, stock } = req.body;
        let producto = await product.findById(req.params.id);
        if (!producto) {
            res.status(404).json({ msg: "no existe el producto" })
        }

        producto.nombre = nombre;
        producto.descripcion =descripcion;
        producto.precio = precio;
        producto.stock = stock;

        producto = await product.findOneAndUpdate({ _id: req.params.id }, producto, { new: true })
        res.json(producto);
    } catch (error) {
        console.log(error);
        res.status(500).send('hubo un error');

    }
}

// obtenemos un producto
exports.obtenerProducto = async(req, res) => {

    try {
        let producto = await product.findById(req.params.id);
        if (!producto) {
            res.status(404).json({ msg: "no existe el producto" })
        }

        res.json(producto);

    } catch (error) {
        console.log(error);
        res.status(500).send('hubo un error');

    }
}

//Eliminamos producto
exports.eliminarProducto = async(req, res) => {

    try {
        let producto = await product.findById(req.params.id);

        if (!producto) {
            res.status(404).json({ msg: "no existe el producto" })
        }

        await product.findOneAndRemove({ _id: req.params.id })
        res.json({ msg: "producto eliminado con exito" });

    } catch (error) {
        console.log(error);
        res.status(500).send('hubo un error ');

    }
}