const estilista = require("../models/estilista");

//Creamos estilista
exports.crearEstilista = async(req, res) => {

    try {
        let stilista;
        //creamos un nuevo estilista
        stilista = new estilista(req.body);

        await stilista.save();
        res.send(stilista);

    } catch (error) {

        console.log(error);
        res.status(500).send('hubo un error');
    }
}

//Obtenemos estilistas
exports.obtenerEstilistas = async(req, res) => {

    try {
        const stilistas = await estilista.find();
        res.json(stilistas)
    } catch (error) {
        console.log(error);
        res.status(500).send('hubo un error');
    }
}

//Actualizamos estilista
exports.actualizarEstilista = async(req, res) => {

    try {
        const { nombre, apellido, edad, correo, telefono, password, direccion, foto } = req.body;
        let stilista = await estilista.findById(req.params.id);
        if (!stilista) {
            res.status(404).json({ msg: "no existe la Estilista" })
        }

        stilista.nombre = nombre;
        stilista.apellido = apellido;
        stilista.edad = edad;
        stilista.correo = correo;
        stilista.telefono = telefono;
        stilista.password = password;
        stilista.direccion = direccion;
        stilista.foto = foto;

        stilista = await estilista.findOneAndUpdate({ _id: req.params.id }, stilista, { new: true })
        res.json(stilista);
    } catch (error) {
        console.log(error);
        res.status(500).send('hubo un error');

    }
}

// obtenemos una estilista
exports.obtenerEstilista = async(req, res) => {

    try {
        let stilista = await estilista.findById(req.params.id);
        if (!stilista) {
            res.status(404).json({ msg: "no existe la Estilista" })
        }

        res.json(stilista);

    } catch (error) {
        console.log(error);
        res.status(500).send('hubo un error');

    }
}

//Eliminamos estilista
exports.eliminarEstilista = async(req, res) => {

    try {
        let stilista = await estilista.findById(req.params.id);

        if (!stilista) {
            res.status(404).json({ msg: "no existe la Estilista" })
        }

        await estilista.findOneAndRemove({ _id: req.params.id })
        res.json({ msg: "Estilista eliminada con exito" });

    } catch (error) {
        console.log(error);
        res.status(500).send('hubo un error ');

    }
}