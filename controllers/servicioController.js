const service = require("../models/servicios");

//Creamos servicio
exports.crearServicio = async(req, res) => {

    try {
        let servicio;
        //creamos la cita
        servicio = new service(req.body);

        await servicio.save();
        res.send(servicio);

    } catch (error) {

        console.log(error);
        res.status(500).send('hubo un error');
    }
}

//Obtenemos servicios
exports.obtenerServicios = async(req, res) => {

    try {
        const servicio = await service.find();
        res.json(servicio)
    } catch (error) {
        console.log(error);
        res.status(500).send('hubo un error');
    }
}

//Actualizamos servicio
exports.actulizarServicio = async(req, res) => {

    try {
        const { nombre, descripcion, costo, tipo, foto } = req.body;
        let servicio = await service.findById(req.params.id);
        if (!servicio) {
            res.status(404).json({ msg: "no existe el servicio" })
        }

        servicio.nombre = nombre;
        servicio.descripcion = descripcion;
        servicio.costo = costo;
        servicio.tipo = tipo;
        servicio.foto = foto;

        servicio = await service.findOneAndUpdate({ _id: req.params.id }, servicio, { new: true })
        res.json(servicio);
    } catch (error) {
        console.log(error);
        res.status(500).send('hubo un error');

    }
}

// obtenemos un servicio
exports.obtenerServicio = async(req, res) => {

    try {
        let servicio = await service.findById(req.params.id);
        if (!servicio) {
            res.status(404).json({ msg: "no existe el servicio" })
        }

        res.json(servicio);

    } catch (error) {
        console.log(error);
        res.status(500).send('hubo un error');

    }
}

//Eliminamos servicio
exports.eliminarServicio = async(req, res) => {

    try {
        let servicio = await service.findById(req.params.id);

        if (!servicio) {
            res.status(404).json({ msg: "no existe el servicio" })
        }

        await service.findOneAndRemove({ _id: req.params.id })
        res.json({ msg: "servicio eliminado con exito" });

    } catch (error) {
        console.log(error);
        res.status(500).send('hubo un error ');

    }
}