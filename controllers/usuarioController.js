const user = require("../models/usuario");

//Creamos ususario
exports.crearUser = async(req, res) => {

    try {
        let usuario;
        //creamos el usuario
        usuario = new user(req.body);

        await usuario.save();
        res.send(usuario);

    } catch (error) {

        console.log(error);
        res.status(500).send('hubo un error');
    }
}

//Obtenemos usuarios
exports.obtenerUsers = async(req, res) => {

    try {
        const usuarios = await user.find();
        res.json(usuarios)
    } catch (error) {
        console.log(error);
        res.status(500).send('hubo un error');
    }
}

//Actualizamos usuario
exports.actualizarUser = async(req, res) => {

    try {
        const { nombre, apellido, correo, password } = req.body;
        let usuario = await user.findById(req.params.id);
        if (!usuario) {
            res.status(404).json({ msg: "no existe el usuario" })
        }

        usuario.nombre = nombre;
        usuario.apellido = apellido;
        usuario.correo = correo;
        usuario.password = password;

        usuario = await user.findOneAndUpdate({ _id: req.params.id }, usuario, { new: true })
        res.json(usuario);
    } catch (error) {
        console.log(error);
        res.status(500).send('hubo un error');

    }
}

// obtenemos un usuario
exports.obtenerUser = async(req, res) => {

    try {
        let usuario = await user.findById(req.params.id);
        if (!usuario) {
            res.status(404).json({ msg: "no existe el usuario" })
        }

        res.json(usuario);

    } catch (error) {
        console.log(error);
        res.status(500).send('hubo un error');

    }
}

//Eliminamos un usuario
exports.eliminarUser = async(req, res) => {

    try {
        let usuario = await user.findById(req.params.id);

        if (!usuario) {
            res.status(404).json({ msg: "no existe el usuario" })
        }

        await user.findOneAndRemove({ _id: req.params.id })
        res.json({ msg: "Usuario eliminado con exito" });

    } catch (error) {
        console.log(error);
        res.status(500).send('hubo un error ');

    }
}